# republikr

<a href="https://cran.r-project.org/package=republikr" class="pkgdown-release"><img src="https://r-pkg.org/badges/version/republikr" alt="CRAN Status" /></a>

republikr provides functions to scrape article content and metadata from the Swiss online newspaper Republik.

## Details

First of all: This package doesn’t allow any unauthenticated access to the online newspaper. You have to be a (paying) [subscriber of Republik](https://www.republik.ch/angebote). This allows you to [log in to the site](https://www.republik.ch/anmelden) in order to have a session cookie created needed for authentication. This cookie is named `connect.sid` and its value (a cryptographic hash)[^1] must be provided as the `auth_cookie` argument to most of this package’s functions to work properly.

### Articles

Republik categorizes a significant part of its content into sections called [*formats*](https://www.republik.ch/formate). Currently, the following formats are available:

| format                                                                                          |
|:------------------------------------------------------------------------------------------------|
| [`7-uhr-newsletter`](https://www.republik.ch/format/7-uhr-newsletter)                           |
| [`am-gericht`](https://www.republik.ch/format/am-gericht)                                       |
| [`am-klavier`](https://www.republik.ch/format/am-klavier)                                       |
| [`am-wegesrand`](https://www.republik.ch/format/am-wegesrand)                                   |
| [`an-der-bar`](https://www.republik.ch/format/an-der-bar)                                       |
| [`an-die-verlagsetage`](https://www.republik.ch/format/an-die-verlagsetage)                     |
| [`ansichten-aus-afrika`](https://www.republik.ch/format/ansichten-aus-afrika)                   |
| [`auf-lange-sicht`](https://www.republik.ch/format/auf-lange-sicht)                             |
| [`aus-der-arena`](https://www.republik.ch/format/aus-der-arena)                                 |
| [`aus-der-redaktion`](https://www.republik.ch/format/aus-der-redaktion)                         |
| [`bergs-nerds`](https://www.republik.ch/format/bergs-nerds)                                     |
| [`binswanger`](https://www.republik.ch/format/binswanger)                                       |
| [`blattkritik`](https://www.republik.ch/format/blattkritik)                                     |
| [`bookmark`](https://www.republik.ch/format/bookmark)                                           |
| [`briefing-aus-bern`](https://www.republik.ch/format/briefing-aus-bern)                         |
| [`buchclub`](https://www.republik.ch/format/buchclub)                                           |
| [`covid-19-uhr-newsletter`](https://www.republik.ch/format/covid-19-uhr-newsletter)             |
| [`ctrl-alt-r`](https://www.republik.ch/format/ctrl-alt-r)                                       |
| [`das-leben-spielt`](https://www.republik.ch/format/das-leben-spielt)                           |
| [`debatte`](https://www.republik.ch/format/debatte)                                             |
| [`der-der-du-niemals-sein-wirst`](https://www.republik.ch/format/der-der-du-niemals-sein-wirst) |
| [`echo`](https://www.republik.ch/format/echo)                                                   |
| [`eidgenoessische-randnotizen`](https://www.republik.ch/format/eidgenoessische-randnotizen)     |
| [`entwicklungslabor`](https://www.republik.ch/format/entwicklungslabor)                         |
| [`feuilleton-newsletter`](https://www.republik.ch/format/feuilleton-newsletter)                 |
| [`film`](https://www.republik.ch/format/film)                                                   |
| [`fotobuch`](https://www.republik.ch/format/fotobuch)                                           |
| [`genossenschaftsrat`](https://www.republik.ch/format/genossenschaftsrat)                       |
| [`geschmacksache`](https://www.republik.ch/format/geschmacksache)                               |
| [`gift-und-galle`](https://www.republik.ch/format/gift-und-galle)                               |
| [`happening`](https://www.republik.ch/format/happening)                                         |
| [`helfen-sie-mit`](https://www.republik.ch/format/helfen-sie-mit)                               |
| [`herd-und-hof`](https://www.republik.ch/format/herd-und-hof)                                   |
| [`humane-ressourcen`](https://www.republik.ch/format/humane-ressourcen)                         |
| [`kiyaks-exil`](https://www.republik.ch/format/kiyaks-exil)                                     |
| [`klang`](https://www.republik.ch/format/klang)                                                 |
| [`kunst`](https://www.republik.ch/format/kunst)                                                 |
| [`lieblingsgeschichten`](https://www.republik.ch/format/lieblingsgeschichten)                   |
| [`nahr`](https://www.republik.ch/format/nahr)                                                   |
| [`operation-nabucco`](https://www.republik.ch/format/operation-nabucco)                         |
| [`poesie-prosa`](https://www.republik.ch/format/poesie-prosa)                                   |
| [`preis-der-republik`](https://www.republik.ch/format/preis-der-republik)                       |
| [`project-r-newsletter`](https://www.republik.ch/format/project-r-newsletter)                   |
| [`raumdeutung`](https://www.republik.ch/format/raumdeutung)                                     |
| [`republik-live`](https://www.republik.ch/format/republik-live)                                 |
| [`salon-der-republik`](https://www.republik.ch/format/salon-der-republik)                       |
| [`sehfeld`](https://www.republik.ch/format/sehfeld)                                             |
| [`strassberg`](https://www.republik.ch/format/strassberg)                                       |
| [`tech-podcast`](https://www.republik.ch/format/tech-podcast)                                   |
| [`theater`](https://www.republik.ch/format/theater)                                             |
| [`theaterspektakel`](https://www.republik.ch/format/theaterspektakel)                           |
| [`theorie-praxis`](https://www.republik.ch/format/theorie-praxis)                               |
| [`update`](https://www.republik.ch/format/update)                                               |
| [`was-diese-woche-wichtig-war`](https://www.republik.ch/format/was-diese-woche-wichtig-war)     |
| [`was-kommt`](https://www.republik.ch/format/was-kommt)                                         |
| [`watchblog`](https://www.republik.ch/format/watchblog)                                         |
| [`welt-in-serie`](https://www.republik.ch/format/welt-in-serie)                                 |
| [`wochenend-newsletter`](https://www.republik.ch/format/wochenend-newsletter)                   |
| [`wochenrevue`](https://www.republik.ch/format/wochenrevue)                                     |
| [`zur-aktualitaet`](https://www.republik.ch/format/zur-aktualitaet)                             |

The unique identifier for an article is always its hyperlink (`href`).

## Installation

To install the latest development version of republikr, run the following in R:

``` r
if (!("remotes" %in% rownames(installed.packages()))) {
  install.packages(pkgs = "remotes",
                   repos = "https://cloud.r-project.org/")
}

remotes::install_gitlab(repo = "salim_b/r/pkgs/republikr")
```

## Development

### R Markdown format

This package’s source code is written in the [R Markdown](https://rmarkdown.rstudio.com/) file format to facilitate practices commonly referred to as [*literate programming*](https://en.wikipedia.org/wiki/Literate_programming). It allows the actual code to be freely mixed with explanatory and supplementary information in expressive Markdown format instead of having to rely on [`#` comments](https://rstudio.github.io/r-manuals/r-lang/Parser.html#comments) only.

All the `.gen.R` suffixed R source code found under [`R/`](https://gitlab.com/salim_b/r/pkgs/republikr/-/tree/master/R/) is generated from the respective R Markdown counterparts under [`Rmd/`](https://gitlab.com/salim_b/r/pkgs/republikr/-/tree/master/Rmd/) using [`pkgpurl::purl_rmd()`](https://pkgpurl.rpkg.dev/dev/reference/purl_rmd.html)[^2]. Always make changes only to the `.Rmd` files – never the `.R` files – and then run `pkgpurl::purl_rmd()` to regenerate the R source files.

### Coding style

This package borrows a lot of the [Tidyverse](https://www.tidyverse.org/) design philosophies. The R code is guided by the [Tidy design principles](https://design.tidyverse.org/) and is formatted according to the [Tidyverse Style Guide](https://style.tidyverse.org/) (TSG) with the following exceptions:

-   Line width is limited to **160 characters**, double the [limit proposed by the TSG](https://style.tidyverse.org/syntax.html#long-lines) (80 characters is ridiculously little given today’s high-resolution wide screen monitors).

    Furthermore, the preferred style for breaking long lines differs. Instead of wrapping directly after an expression’s opening bracket as [suggested by the TSG](https://style.tidyverse.org/syntax.html#long-lines), we prefer two fewer line breaks and indent subsequent lines within the expression by its opening bracket:

    ``` r
    # TSG proposes this
    do_something_very_complicated(
      something = "that",
      requires = many,
      arguments = "some of which may be long"
    )

    # we prefer this
    do_something_very_complicated(something = "that",
                                  requires = many,
                                  arguments = "some of which may be long")
    ```

    This results in less vertical and more horizontal spread of the code and better readability in pipes.

-   Usage of [magrittr’s compound assignment pipe-operator `%<>%`](https://magrittr.tidyverse.org/reference/compound.html) is desirable[^3].

-   Usage of [R’s right-hand assignment operator `->`](https://rdrr.io/r/base/assignOps.html) is not allowed[^4].

-   R source code is *not* split over several files as [suggested by the TSG](https://style.tidyverse.org/package-files.html) but instead is (as far as possible) kept in the single file [`Rmd/republikr.Rmd`](https://gitlab.com/salim_b/r/pkgs/republikr/-/tree/master/Rmd/republikr.Rmd) which is well-structured thanks to its [Markdown support](#r-markdown-format).

As far as possible, these deviations from the TSG plus some additional restrictions are formally specified in [`pkgpurl::default_linters`](https://pkgpurl.rpkg.dev/reference/default_linters), which is (by default) used in [`pkgpurl::lint_rmd()`](https://pkgpurl.rpkg.dev/reference/lint_rmd), which in turn is the recommended way to lint this package.

[^1]: How you can access the locally stored cookies of a specific site in Firefox is described [here](https://developer.mozilla.org/docs/Tools/Storage_Inspector), the same for Google Chrome [here](https://developers.google.com/web/tools/chrome-devtools/storage/cookies).

[^2]: The very idea to leverage the R Markdown format to author R packages was originally proposed by Yihui Xie. See his excellent [blog post](https://yihui.org/rlp/) for his point of view on the advantages of literate programming techniques and some practical examples. Note that using `pkgpurl::purl_rmd()` is a less cumbersome alternative to the Makefile approach outlined by him.

[^3]: The TSG [explicitly instructs to avoid this operator](https://style.tidyverse.org/pipes.html#assignment-2) – presumably because it’s relatively unknown and therefore might be confused with the forward pipe operator `%>%` when skimming code only briefly. I don’t consider this to be an actual issue since there aren’t many sensible usage patterns of `%>%` at the beginning of a pipe sequence inside a function – I can only think of creating side effects and relying on [R’s implicit return of the last evaluated expression](https://rdrr.io/r/base/function.html). Therefore – and because I really like the `%<>%` operator – it’s usage is welcome.

[^4]: The TSG [explicitly accepts `->` for assignments at the end of a pipe sequence](https://style.tidyverse.org/pipes.html#assignment-2) while Google’s R Style Guide [considers this bad practice](https://google.github.io/styleguide/Rguide.html#right-hand-assignment) because it “makes it harder to see in code where an object is defined”. I second the latter.
